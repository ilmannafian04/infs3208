# A2 notes

## How to Run

The flag `-d` stands for detached, if you want to see the output, you can remove
that flag.
```shell script
docker-compose up -d
```

## How to Stop

If you started the compose with the flag `-d`, you can stop the compose by sending
an interrupt signal `ctrl+c`.
```shell script
docker-compose down
```

## Explanation on `.env`

Any sensitive information shouldn't be hard coded in the application. In our case,
hardcoding the `MYSQL_ROOT_PASSWORD` and `MYSQL_PASSWORD` is dangerous and is a bad
practice. To solve this, we can use the `env_file` scalar in the compose file that
point to an `.env` file. I left both password entry in the compose file in case
using `env_file` is not allowed.
